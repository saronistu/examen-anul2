<?php

namespace App\Repository;

use App\Entity\Stud;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Stud|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stud|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stud[]    findAll()
 * @method Stud[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Stud::class);
    }

    // /**
    //  * @return Stud[] Returns an array of Stud objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stud
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
