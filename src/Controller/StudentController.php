<?php

namespace App\Controller;

use App\Entity\Students;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class StudentController extends AbstractController
{
    /**
     * @Route("/student", name="student")
     */
    public function createStudents(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $students = new Students();
        $students->setFirstName('Saron');
        $students->setLastName('Ciupe');
        $students->setEmail('saronistu@gmail.com');

        $entityManager->persist($students);
        $entityManager->flush();

        return new Response('Saved new student with id '.$students->getId());
    }
      /**
     * @Route("/student/{id}", name="student_show")
     */
    public function show($id)
{
    $student = $this->getDoctrine()
        ->getRepository(Students::class)
        ->find($id);

    if (!$student) {
        throw $this->createNotFoundException(
            'No student found for id '.$id
        );
    }

    return new Response('Check out this great student: '.$student->getFirstName());
}
  /**
     * @Route("/student/count", name="student_count")
     */
public function count()
{
    $student = Student::all();

    $allStudentsWithStudentCount = [
        'student' => $student,
        'products_count' => $student->count()
    ];

    return $allStudentsWithStudentCount;
}
}
