<?php

namespace App\Controller;

use App\Entity\Professors;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class ProfessorsController extends AbstractController
{
    /**
     * @Route("/professors", name="professors")
     */
    public function createProfessors(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $professor = new Professors();
        $professor->setFirstName('Radu');
        $professor->setLastName('Crisan');
        $professor->setEmail('raducrisan@gmail.com');

        $entityManager->persist($professor);
        $entityManager->flush();

        return new Response('Saved new professor with id '.$professor->getId());
    }
}
